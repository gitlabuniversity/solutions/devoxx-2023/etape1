Nice mermaid diagram

```mermaid

flowchart TD
    A[Devoxx] -->|Get accepted| B(Do workshop)
    B --> C{Let me think}
    C -->|One| D[A success]
    C -->|Two| E[Some mistakes]
    C -->|Three| F[A disaster]
  
```